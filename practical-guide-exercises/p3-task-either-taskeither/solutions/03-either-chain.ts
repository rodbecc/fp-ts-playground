/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - Modify multiplyPositiveFractionNumberBy5 to add a call to fractionNumber here using E.chainW
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 03-either-chain.ts
 *
 * Testing this file.
 *  - npm run test 03-either-chain
 */

import * as E from 'fp-ts/lib/Either'
import { flow, pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

class PositiveNumberError extends Error {
  public readonly type = 'POSITIVE_NUMBER_ERROR'
}

class FractionNumberError extends Error {
  public readonly type = 'FRACTION_NUMBER_ERROR'
}

const positiveNumber = (number: number): E.Either<PositiveNumberError, number> =>
  number < 0 ? E.left(new PositiveNumberError(`${number} is not positive.`)) : E.right(number)

const fractionNumber = (number: number): E.Either<FractionNumberError, number> =>
  number <= -1 || number >= 1
    ? E.left(new FractionNumberError(`${number} is not a fraction.`))
    : E.right(number)

export const multiplyPositiveFractionNumberBy5 = flow(
  positiveNumber,
  E.chainW(fractionNumber),
  E.map(num => num * 5)
)

// No need to modify below here, for running this file
const logResult = (number: number) =>
  pipe(console.dir(`multiplyPositiveFractionNumberBy5(${number}):`), () =>
    console.dir(multiplyPositiveFractionNumberBy5(number))
  )

pipe(
  T.fromIO(() =>
    pipe(
      logResult(10),
      () => logResult(-0.12),
      () => logResult(0.12)
    )
  ),
  runIfCli(module)
)
