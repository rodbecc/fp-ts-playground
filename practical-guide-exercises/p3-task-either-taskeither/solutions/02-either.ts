/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - Modify positiveNumber to remove the throw and instead return Either<PositiveNumberError, number>
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 02-either.ts
 *
 * Testing this file.
 *  - npm run test 02-either
 */

import * as E from 'fp-ts/lib/Either'
import { flow, pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

class PositiveNumberError extends Error {
  public readonly type = 'POSITIVE_NUMBER_ERROR'
}

// TODO Modify positiveNumber to remove the throw and instead return Either<PositiveNumberError, number>
// If number is less than 0, return E.left(new PositiveNumberError(`${number} is not positive.`))
// If number is greater than or equal to 0, return E.right(number)
const positiveNumber = (number: number): E.Either<PositiveNumberError, number> =>
  number < 0 ? E.left(new PositiveNumberError(`${number} is not positive.`)) : E.right(number)

const positiveNumberAlternate = E.fromPredicate(
  (num: number) => num >= 0,
  num => new PositiveNumberError(`${num} is not positive.`)
)

// TODO fixing the positiveNumber function should fix the type error here
export const multiplyPositiveNumberBy5 = flow(
  positiveNumber,
  E.map(num => num * 5)
)

// No need to modify below here, for running this file
const logResult = (number: number) =>
  pipe(console.dir(`multiplyPositiveNumberBy5(${number}):`), () =>
    console.dir(multiplyPositiveNumberBy5(number))
  )

pipe(
  T.fromIO(() => pipe(logResult(10), () => logResult(-5))),
  runIfCli(module)
)
