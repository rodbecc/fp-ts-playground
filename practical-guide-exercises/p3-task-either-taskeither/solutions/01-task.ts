/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - Modify add5Task to fix the type error by returing a Task<number> instead of number
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 01-task.ts
 *
 * Testing this file.
 *  - npm run test 01-task
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

export const add5Task = (number: number): T.Task<number> => async () => number + 5
export const add5TaskAlternate1 = (number: number): T.Task<number> => () =>
  Promise.resolve(number + 5)
export const add5TaskAlternate2 = (number: number): T.Task<number> => T.fromIO(() => number + 5)
// Note: for the following T.of solution, the calculation is evaluated when add5TaskAlternate3 is called,
// not when the task is invoked, which can lead to unintended effects.  The T.fromIO solution is preferred to this one.
export const add5TaskAlternate3 = (number: number): T.Task<number> => T.of(number + 5)

// No need to modify below here, for running this file
const logAndReturnResult = (number: number) =>
  pipe(
    add5Task(number),
    T.map(result =>
      pipe(
        console.dir(`add5Task(${number}):`),
        () => console.dir(result),
        () => result
      )
    )
  )

pipe(logAndReturnResult(5), T.chain(logAndReturnResult), runIfCli(module))
