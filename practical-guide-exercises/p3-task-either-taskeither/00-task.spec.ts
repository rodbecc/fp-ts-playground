import { add5Task } from './00-task'

describe('00-task', () => {
  it('add5Task returns an task with the proper calculation', async () => {
    const result = await add5Task(10)()
    expect(result).toStrictEqual(15)
  })
})
