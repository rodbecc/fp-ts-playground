/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - You have decided that you'd like to return -1 when there is an error
 *    from getBalanceFromMockApi rather than returning a left with an error.
 *    Modify getBalanceTask using TE.fold or TE.getOrElse to convert the TaskEither
 *    into a Task, returning -1 if the TaskEither is left, and the normal balance otherwise
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 06-task-either.ts
 *
 * Testing this file.
 *  - npm run test 06-task-either-fold
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import * as TE from 'fp-ts/lib/TaskEither'
import { runIfCli } from '../utils'

class GetBalanceError extends Error {
  public readonly type = 'GET_BALANCE_ERROR'
  constructor(public readonly error: unknown) {
    super()
  }
}

type AccountId = 'account.123' | 'account.456'

const getBalanceFromMockApi = async (accountId: AccountId) => {
  if (accountId === 'account.456') {
    throw new Error('bad account')
  }
  return 1023.52
}

// TODO you have decided that you'd like to return -1 when there is an error
// from getBalanceFromMockApi rather than returning a left with an error.
// Modify getBalanceTask using TE.fold or TE.getOrElse to convert the TaskEither
// into a Task, returning -1 if the TaskEither is left, and the normal balance otherwise
export const getBalanceTask = (accountId: AccountId): T.Task<number> =>
  TE.tryCatch(
    () => getBalanceFromMockApi(accountId),
    e => new GetBalanceError(e)
  )

// No need to modify below here, for running this file
const logResult = (accountId: AccountId) =>
  pipe(
    T.fromIO(() => console.dir(`getBalanceFromMockApi(${accountId}):`)),
    T.chain(() => getBalanceTask(accountId)),
    T.map(console.dir)
  )

pipe(['account.123', 'account.456'], T.traverseSeqArray(logResult), runIfCli(module))
