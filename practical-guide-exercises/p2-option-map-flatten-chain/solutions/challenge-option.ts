/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify add5Subtract1 to return Option<NumberObject> instead of NumberObjectPartial
 */

import { sequenceS } from 'fp-ts/lib/Apply'
import { pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

interface NumberObject {
  number: number
}

// This can also be express as Partial<NumberObject>
interface NumberObjectPartial {
  number?: number
}

const add5 = (numObj: NumberObject) => ({ number: numObj.number + 5 })
const subtract1 = (numObj: NumberObject) => ({ number: numObj.number - 1 })

export const add5Subtract1 = (numberObjectPartial: NumberObjectPartial) =>
  pipe(
    O.fromNullable(numberObjectPartial.number),
    O.map(number => ({ number })),
    O.map(add5),
    O.map(subtract1)
  )

export const add5Subtract1AlternateSolution = (numberObjectPartial: NumberObjectPartial) =>
  pipe(
    sequenceS(O.option)({
      number: O.fromNullable(numberObjectPartial.number),
    }),
    O.map(add5),
    O.map(subtract1)
  )

// No need to modify below here, for running this file
const logAdd5Subtract1 = (numObj?: NumberObjectPartial) =>
  pipe(console.dir(`add5Subtract1(${JSON.stringify(numObj)}):`), () =>
    console.dir(add5Subtract1(numObj))
  )

pipe(
  T.fromIO(() => pipe(logAdd5Subtract1({ number: 10 }), () => logAdd5Subtract1({}))),
  runIfCli(module)
)
