/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify add5Subtract1 to return Option<NumberObject> instead of NumberObjectPartial
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node challenge-option.ts
 *
 * Testing this file.
 *  - npm run test challenge-option
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../utils'

interface NumberObject {
  number: number
}

// This can also be express as Partial<NumberObject>
interface NumberObjectPartial {
  number?: number
}

const add5 = (numObj: NumberObject) => ({ number: numObj.number + 5 })
const subtract1 = (numObj: NumberObject) => ({ number: numObj.number - 1 })

// TODO Modify add5Subtract1 to return Option<NumberObject> instead of NumberObjectPartial
export const add5Subtract1 = (numberObjectPartial: NumberObjectPartial) =>
  pipe(
    numberObjectPartial,
    numObj => (numObj.number !== undefined ? add5({ number: numObj.number }) : numObj),
    numObj => (numObj.number !== undefined ? subtract1({ number: numObj.number }) : numObj)
  )

// No need to modify below here, for running this file
const logAdd5Subtract1 = (numObj?: NumberObjectPartial) =>
  pipe(console.dir(`add5Subtract1(${JSON.stringify(numObj)}):`), () =>
    console.dir(add5Subtract1(numObj))
  )

pipe(
  T.fromIO(() => pipe(logAdd5Subtract1({ number: 10 }), () => logAdd5Subtract1({}))),
  runIfCli(module)
)
