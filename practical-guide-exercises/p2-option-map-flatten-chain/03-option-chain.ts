/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify multiplyPositiveNumberBy5 to return Option<number> instead of Option<Option<number>>
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 03-option-chain.ts
 *
 * Testing this file.
 *  - npm run test 03-option-chain
 */

import { pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../utils'

const positiveNumber = (number: number) => (number < 0 ? O.none : O.some(number))

// TODO Modify multiplyPositiveNumberBy5 to return Option<number> instead of Option<Option<number>>
// TODO using O.chain
export const multiplyPositiveNumberBy5 = (number?: number) =>
  pipe(O.fromNullable(number), O.map(positiveNumber), O.map(O.map(num => num * 5)))

// No need to modify below here, for running this file
const logResult = (number?: number) =>
  pipe(console.dir(`multiplyPositiveNumberBy5(${number}):`), () =>
    console.dir(multiplyPositiveNumberBy5(number))
  )

pipe(
  T.fromIO(() =>
    pipe(
      logResult(10),
      () => logResult(-5),
      () => logResult()
    )
  ),
  runIfCli(module)
)
