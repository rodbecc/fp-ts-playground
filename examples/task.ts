import { pipe } from "fp-ts/lib/function"
import * as T from "fp-ts/lib/Task"

const getUser = (userId: string): T.Task<string> => async () => userId

const log = (message: string) => pipe(console.log(message), () => message)

const getTwoUsers = (userId1: string, userId2: string) =>
  pipe(
    getUser(userId1),
    T.map(log),
    T.chain(() => getUser(userId2)),
    T.map(log)
  )

const task = getTwoUsers("user.123", "user.234")

task()
